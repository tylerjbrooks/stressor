/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './stressor -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#ifndef XNOR_H
#define XNOR_H

#include <string>
#include <queue>
#include <memory>
#include <atomic>
#include <thread>
#include <mutex>
#include <future>

#include "utils.h"
#include "base.h"
#include "encoder.h"

extern "C" {
#include "xnornet.h"
}

namespace stressor {

class Xnor : public Base, Base::Listener {
  public:
    static std::unique_ptr<Xnor> create(unsigned int yield_time, bool quiet, 
        Encoder* enc,  unsigned int width, unsigned int height, unsigned int engines);
    virtual ~Xnor();

  public:
    virtual bool addMessage(Base::Listener::Message msg, void* data);

  protected:
    Xnor() = delete;
    Xnor(unsigned int yield_time);
    bool init(bool quiet, Encoder* enc, unsigned int width, 
        unsigned int height, unsigned int engines);
    bool deinit();

  protected:
    virtual bool waitingToRun();
    virtual bool running();
    virtual bool paused();
    virtual bool waitingToHalt();

  private:
    bool quiet_;
    Encoder* enc_;
    unsigned int width_;
    unsigned int height_;
    unsigned int luma_len_;
    unsigned int chrom_len_;

    int obj_max_ = 5;

    class Frame {
      public:
        Frame() = delete;
        Frame(char c, xnor_model* m) 
          : name(c), model(m), scratch(nullptr),
            differ_image(), differ_eval(), 
            fut() {}
        ~Frame() {}
      public:
        char name;
        xnor_model* model;
        std::shared_ptr<Base::Listener::ScratchBuf> scratch;
        Differ differ_image;
        Differ differ_eval;
        std::future<xnor_evaluation_result*> fut;
    };
    std::timed_mutex engine_lock_;
    unsigned int engine_num_;
    unsigned int frame_len_;
    std::queue<std::shared_ptr<Xnor::Frame>> engine_pool_;
    std::queue<std::shared_ptr<Xnor::Frame>> engine_work_;
    std::queue<std::shared_ptr<Xnor::Frame>> engine_pile_;

    unsigned int post_id_ = 0;
    unsigned int eval_timeout_ = 1000;
    xnor_evaluation_result* eval(Xnor::Frame* frame);
    static xnor_evaluation_result* eval0(Xnor* self, Xnor::Frame* frame);
    Base::Listener::BoxBuf::Type targetType(const char* label);
    bool post(xnor_evaluation_result* result, unsigned int id, bool report);
    bool oneRun(bool report);

    std::atomic<bool> xnor_on_;

    Differ differ_copy_;
    Differ differ_tot_;
};

} // namespace stressor

#endif // XNOR_H
