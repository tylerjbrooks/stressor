# environment variables:
#   RASPBIAN is the location of your raspbian projects
#   RASPBIANCROSS is the location of the raspbian compiler tools
#   XNORPLATFORM is the xnor supported platform: like 'rpi0' or 'rpi3'
#   XNORMODEL is the xnor model: like 'person-pet-vehicle-detector'
#   XNORSDK is the location of your xnor sdk
#   LIBYUV is the location of your libyuv
#   OMXSUPPORT is the location of your 'video core' support (OMX and such)
#   LD_LIBRARAY_PATH is the loation of your xnor model library

CXX = $(RASPBIANCROSS)g++

SRC = stressor.cpp base.cpp capturer.cpp xnor.cpp encoder.cpp rtsp.cpp utils.cpp
OBJ = $(SRC:.cpp=.o)
EXE = stressor

# Turn on 'CAPTURE_ONE_RAW_FRAME' to write the 10th frame
# in to './frame_wxh_ffps.yuv' file (w=width, h=height, f=framerate).
#
# Turn on 'OUTPUT_VARIOUS_BITS_OF_INFO' to print out various
# bits of interesting information about the setup.
#
# Turn on 'DEBUG_MESSAGES' to turn on debug messages.
#FEATURES = -DCAPTURE_ONE_RAW_FRAME -DOUTPUT_VARIOUS_BITS_OF_INFO -DDEBUG_MESSAGES

CFLAGS =-DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -DTARGET_POSIX -D_LINUX -fPIC -DPIC -D_REENTRANT -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -Wall -g -DHAVE_LIBOPENMAX=2 -DOMX -DOMX_SKIP64BIT -ftree-vectorize -pipe -DUSE_EXTERNAL_OMX -DHAVE_LIBBCM_HOST -DUSE_EXTERNAL_LIBBCM_HOST -DUSE_VCHIQ_ARM -std=c++11 -Wno-psabi -Dxnorver_$(XNORVER) -Dxnorplat_$(XNORPLATFORM) $(FEATURES)

LDFLAGS =-L$(OMXSUPPORT)/lib -L$(XNORSDK)/xnorver_$(XNORVER)/lib/$(XNORPLATFORM)/$(XNORMODEL) -L$(LIBYUV) -L$(LIVE555)/liveMedia -L$(LIVE555)/UsageEnvironment -L$(LIVE555)/BasicUsageEnvironment -L$(LIVE555)/groupsock

LIBS =-lxnornet -lyuv -lliveMedia -lgroupsock -lBasicUsageEnvironment -lUsageEnvironment 
LIBS += -lopenmaxil -lbcm_host -lvcos -lvchiq_arm -lbrcmEGL -lbrcmGLESv2 -lpthread -lrt -lm

INCLUDES =-I. -I$(OMXSUPPORT)/include -I$(XNORSDK)/xnorver_$(XNORVER)/include -I$(LIBYUV)/include -I$(LIVE555)/liveMedia/include -I$(LIVE555)/UsageEnvironment/include -I$(LIVE555)/BasicUsageEnvironment/include -I$(LIVE555)/groupsock/include

$(EXE): $(OBJ)
	$(CXX) $(LDFLAGS) $(OBJ) $(LIBS) -o $@

.cpp.o:
	$(CXX) $(CFLAGS) $(INCLUDES) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJ)

