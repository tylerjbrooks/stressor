/*
 * Copyright © 2019 Tyler J. Brooks <tylerjbrooks@digispeaker.com> <https://www.digispeaker.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * <http://www.apache.org/licenses/LICENSE-2.0>
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Try './stressor -h' for usage.
 *
 * This program uses Ai2Go from XNOR.ai (http://xnor.ai)
 */

#include <chrono>
#include <cstring>
#include <algorithm>

#include "xnor.h"

namespace stressor {

Xnor::Xnor(unsigned int yield_time)
  : Base(yield_time) {
}

Xnor::~Xnor() {
  deinit();
}

std::unique_ptr<Xnor> Xnor::create(unsigned int yield_time, bool quiet, 
    Encoder* enc, unsigned int width, unsigned int height, unsigned int engines) {
  auto obj = std::unique_ptr<Xnor>(new Xnor(yield_time));
  obj->init(quiet, enc, width, height, engines);
  return obj;
}

bool Xnor::init(bool quiet, Encoder* enc, unsigned int width, 
    unsigned int height, unsigned int engines) {

  quiet_ = quiet;

  enc_ = enc;
  
  width_ = width;
  height_ = height;

  luma_len_ = ALIGN_16B(width_) * ALIGN_16B(height_);
  chrom_len_ = luma_len_ / 4;

  frame_len_ = ALIGN_16B(width_) * ALIGN_16B(height_) * 3 / 2;

  engine_num_ = engines;

  xnor_on_ = false;

  return true; 
}

bool Xnor::deinit() {
  return true;
}

bool Xnor::addMessage(Base::Listener::Message msg, void* data) {

  if (msg != Base::Listener::Message::kScratchBuf) {
    dbgMsg("xnor message not recognized\n");
    return false;
  }

  std::unique_lock<std::timed_mutex> lck(engine_lock_, std::defer_lock);

  if (!lck.try_lock_for(std::chrono::microseconds(Base::Listener::timeout_))) {
//    dbgMsg("xnor busy\n");
    return false;
  }

  if (engine_pool_.size() == 0) {
//    dbgMsg("no xnor buffers available\n");
    return false;
  }

  auto scratch = *static_cast<std::shared_ptr<Base::Listener::ScratchBuf>*>(data);
  if (frame_len_ != scratch->length) {
    dbgMsg("xnor buffer size mismatch\n");
    return false;
  }

  differ_copy_.begin();
  auto frame = engine_pool_.front();
  engine_pool_.pop();
  frame->scratch = scratch;
  engine_work_.push(frame);
  differ_copy_.end();

  return true;
}

bool Xnor::waitingToRun() {

  if (!xnor_on_) {

    for (unsigned int i = 0; i < engine_num_; i++) {
#if defined(xnorver_0_9)
      // xnor options
      xnor_model_load_options* opts;
      opts = xnor_model_load_options_create();
      if (opts == nullptr) {
        dbgMsg("xnor could not create options\n");
        return false;
      }
      xnor_error* err = xnor_model_load_options_set_threading_model(opts, 
          kXnorThreadingModelMultiThreaded);
      if (err != nullptr) {
        dbgMsg("xnor could not set threading model: %s\n",
            xnor_error_get_description(err));
        return false;
      }

      // xnor model
      xnor_model* model = nullptr;
      err = xnor_model_load_built_in("", opts, &model);
      if (err != nullptr) {
        dbgMsg("xnor could not load model: %s\n",
            xnor_error_get_description(err));
        return false;
      }
      xnor_model_load_options_free(opts);
#else
      // xnor model
      xnor_model* model = nullptr;
      xnor_error* err = xnor_model_load_built_in("", nullptr, &model);
      if (err != nullptr) {
        dbgMsg("xnor could not load model: %s\n",
            xnor_error_get_description(err));
        return false;
      }
#endif
      engine_pool_.push(
          std::shared_ptr<Xnor::Frame>(new Xnor::Frame( 'A' + i, model)));
    }

    differ_tot_.begin();
    xnor_on_ = true;
  }

  return true;
}
xnor_evaluation_result* Xnor::eval(Xnor::Frame* frame) {

  // create xnor input
  frame->differ_image.begin();
  xnor_input* input = nullptr;
  xnor_error* err = xnor_input_create_yuv420p_image(
      width_,
      height_, 
      frame->scratch->buf.data(), 
      frame->scratch->buf.data() + luma_len_,
      frame->scratch->buf.data() + luma_len_ + chrom_len_, &input);
  if (err != nullptr) {
    dbgMsg("xnor could not create input: %s\n", 
        xnor_error_get_description(err));
    return nullptr;
  }
  frame->differ_image.end();

  // evaluate with xnor
  frame->differ_eval.begin();
  xnor_evaluation_result* result = nullptr;
  err = xnor_model_evaluate(frame->model, input, nullptr, &result);
  if (err != nullptr) {
    dbgMsg("xnor could not evaluate: %s\n", 
        xnor_error_get_description(err));
    xnor_input_free(input);
    return nullptr;
  }
  frame->differ_eval.end();
  xnor_input_free(input);
  return result;
}

xnor_evaluation_result* Xnor::eval0(Xnor* self, Xnor::Frame* frame) {
  return self->eval(frame);
}

Base::Listener::BoxBuf::Type Xnor::targetType(const char* label)
{
  std::string str = label;
  if (str.compare("person") == 0) {
    return Base::Listener::BoxBuf::Type::kPerson;
  } else if (str.compare("pet") == 0) {
    return Base::Listener::BoxBuf::Type::kPet;
  } else {
    return Base::Listener::BoxBuf::Type::kVehicle;
  }
}

bool Xnor::post(xnor_evaluation_result* result, unsigned int id, bool report) {

  if (result != nullptr) {

    if (xnor_evaluation_result_get_type(result) != 
        kXnorEvaluationResultTypeBoundingBoxes) {
      dbgMsg("xnor not detecting objects\n");

    } else {

//      xnor_bounding_box objects[obj_max_] = {0};
      xnor_bounding_box objects[obj_max_];
      memset(objects, 0, sizeof(objects));
      xnor_evaluation_result_get_bounding_boxes(result, objects, obj_max_);
                   
      auto boxes = std::shared_ptr<std::vector<Base::Listener::BoxBuf>>(
            new std::vector<Base::Listener::BoxBuf>);

      // make vector of boxes
      int cnt = xnor_evaluation_result_get_bounding_boxes(result, nullptr, 0);
      if (cnt != 0) {
        for (int i = 0; i < std::min(cnt,obj_max_); i++) {
#ifdef DEBUG_MESSAGES
          dbgMsg("xnor found on frame %d: %d: %s at x=%f y=%f w=%f h=%f\n",
              id,
              objects[i].class_label.class_id,
              objects[i].class_label.label,
              objects[i].rectangle.x,
              objects[i].rectangle.y,
              objects[i].rectangle.width,
              objects[i].rectangle.height);
#else
          if (report && !quiet_) {
            fprintf(stderr, "<%s>", objects[i].class_label.label);
            fflush(stdout);
          }
#endif
          unsigned int left = objects[i].rectangle.x * width_;
          unsigned int top  = objects[i].rectangle.y * height_;
          unsigned int width  = objects[i].rectangle.width * width_;
          unsigned int height = objects[i].rectangle.height * height_;

          if (left < width_ && top < height_) {
            if (left + width > width_) {
              width = width_ - left;
            }
            if (top + height > height_) {
              height = height_ - top;
            }
            boxes->push_back(Base::Listener::BoxBuf(
                targetType(objects[i].class_label.label),
                objects[i].class_label.class_id,
                id, left, top, width, height));
          }
        }
      }

      // send boxes if new
      if (enc_) {
        if (post_id_ <= id) {
          if (!enc_->addMessage(Base::Listener::Message::kBoxBuf, &boxes)) {
            dbgMsg("xnor target encoder busy\n");
          }
          post_id_ = id;
        }
      }

    }
  }

  return true;
}

bool Xnor::oneRun(bool report) {
  std::unique_lock<std::timed_mutex> lck(engine_lock_);

  // check for new data
  if (engine_work_.size() != 0) {
    auto frame = engine_work_.front();
    engine_work_.pop();

    engine_pile_.push(frame);
    frame->fut = std::async(std::launch::async, Xnor::eval0, this, frame.get());

  // check for data in process
  } else if (engine_pile_.size() != 0) {

    auto frame = engine_pile_.front();

    if (frame->fut.wait_for(
          std::chrono::microseconds(eval_timeout_)) == std::future_status::ready) {
      engine_pile_.pop();

      xnor_evaluation_result* result = frame->fut.get();

      if (result != nullptr) {
        post(result, frame->scratch->id, report);
        xnor_evaluation_result_free(result);
      }
      frame->scratch = std::shared_ptr<Base::Listener::ScratchBuf>(new Base::Listener::ScratchBuf());
      engine_pool_.push(frame);
    }
  }

  return true;
}
bool Xnor::running() {

  if (xnor_on_) {
    return oneRun(true);
  }

//  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  return true;
}

bool Xnor::paused() {
  return true;
}

bool Xnor::waitingToHalt() {

  if (xnor_on_) {
    xnor_on_ = false;
    differ_tot_.end();

    // finish processing
    while (engine_work_.size() || engine_pile_.size()) {
      oneRun(false);
    }

    // report
    if (!quiet_) {
      fprintf(stderr, "\n\nXnor Results...\n");
      fprintf(stderr, "           image copy time (us): high:%u avg:%u low:%u frames:%d\n", 
          differ_copy_.getHigh_usec(), differ_copy_.getAvg_usec(), differ_copy_.getLow_usec(),
          differ_copy_.getCnt());

      std::vector<std::shared_ptr<Xnor::Frame>> vec;
      while (engine_pool_.size()) {
        auto frame = engine_pool_.front();
        engine_pool_.pop();
        vec.push_back(frame);
      }
      std::sort(vec.begin(), vec.end(), 
          [](const std::shared_ptr<Xnor::Frame> & a, 
            const std::shared_ptr<Xnor::Frame> & b) -> bool { return a->name < b->name; });
      unsigned int cnt = 0;
      for (auto& frame : vec) {
        fprintf(stderr, "  buffer %c image prep time (us): high:%u avg:%u low:%u frames:%d\n", 
            frame->name, frame->differ_image.getHigh_usec(), frame->differ_image.getAvg_usec(), 
            frame->differ_image.getLow_usec(), frame->differ_image.getCnt());
        fprintf(stderr, "  buffer %c image eval time (us): high:%u avg:%u low:%u frames:%d\n", 
            frame->name, frame->differ_eval.getHigh_usec(), frame->differ_eval.getAvg_usec(), 
            frame->differ_eval.getLow_usec(), frame->differ_eval.getCnt());
        xnor_model_free(frame->model);
//        fprintf(stderr, "\n");
        cnt += frame->differ_eval.getCnt();
      }
        
      fprintf(stderr, "                total test time: %f sec\n", 
          differ_tot_.getAvg_usec() / 1000000.f);
      fprintf(stderr, "              frames per second: %f fps\n", 
          cnt * 1000000.f / differ_tot_.getAvg_usec());
    }
  }
  return true;
}

} // namespace stressor

