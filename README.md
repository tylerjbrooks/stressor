# Stressor

Stressor is an application that runs a video encoding pipeline from video capture to
an elemental stream file and includes realtime target detection.  Targets are marked 
with boxes in the output file.  The application is constructed in such a way as to allow 
the user to add more 'detection engines' and measure the load on the platform.

Target detection is provided by the Ai2Go SDK from [XNOR](https://www.xnor.ai).

RTSP Server is provided by [Live555](http://www.live555.com);

Below are instructions for building and running Stressor.  

### Prerequisite

This demo requires the Ai2Go SDK from XNOR.ai.  You can sign up on
their website at [XNOR](https://www.xnor.ai).

This demo requires Live555 from [Live555](http://www.live555.com).

Theoretically this demo can be built and run on any platform that supports video4linux2,
XNOR Ai2Go, and OMX hardware encoding. However, at this time I have only tested it on a
[Raspberry Pi 3 b+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus) with a [OV5647 camera module](http://www.arducam.com/product/arducam-ov5647-standard-raspberry-pi-camera-b0033) and on a [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/) with the same camera.

I have added USB camera support so you don't have to buy the camera module.  Just
any webcam will do.

Stick to the smaller resolutions like 640x360 or 640x480 for best performance.  You can use
larger formats like 1280x720 but it slows down the framerate.

### Installation

Get the code at:
```
git clone https://gitlab.com/tylerjbrooks/stressor.git
```

In another directory, get the Google libyuv code:
```
git clone https://chromium.googlesource.com/libyuv/libyuv
```

In another directory, the the Live555 code:
```
git clone https:/gitlab.com/tylerjbrooks/live.git
```

### Build Notes

I build directly on the platform.  I run Raspbian from the NOOB disk on the rpiX platforms.

To build, you will need the following environment variables:
```
XNORPLATFORM=<your-platform: like 'rpi0' or 'rpi3'>
XNORMODEL=<your-model: like 'persion-pet-vehicle-detector'>
XNORVER=<your xnor sdk: like '0_3' or '0_9'>
XNORSDK=/path/to/xnor/sdk
LIBYUV=/path/to/libyuv
OMXSUPPORT=<your omx: like '/opt/vc'>
LIVE555=/path/to/live555
LD_LIBRARY_PATH=/path/to/xnor/xnorver_$XNORVER/lib/$XNORPLATFORM/$XNORMODEL
```

First, build libyuv:
```
cd libyuv
make V=1 -f linux.mk
```

Next, build live555:
```
cd live
./genMakefile linux
make
```

Now build stressor:
```
cd stressor
make
```

Use this to clean:
```
cd stressor
make clean
```

### Usage

Invoke stressor from the command line like this:
```
./stressor stressor -?qrutdfwhbx [output]

version: x.xx
where:
  ?           = this screen"                           
  (q)uiet     = suppress messages   (default = false)
  (r)tsp      = rtsp server         (default = off)  
  (u)nicast   = rtsp unicast addr   (default = none) 
              = multicast if no address specified)
  (t)esttime  = test duration       (default = 30sec)
              = 0 to run until ctrl-c
  (d)device   = video device num    (default = 0)
  (f)ramerate = capture framerate   (default = 20)
  (w)idth     = capture width       (default = 640)
              = negative value means flip
  (h)eight    = capture height      (default = 480)
              = negative value means flip
  (b)itrate   = encoder bitrate     (default = 1000000)
  (x)nor      = xnor engines        (default = 1)
              = 0 for no xnor target detection
  output      = output file name
              = leave blank for stdout
```

As an example, the following command will run the test for 30 seconds with 2 XNOR detection engines.
It will send the output to stdout and also multicast the video with rtsp:
```
./stressor -r -t 30 -x 2 >output.h264
```

Typical output looks like this:
```
Capturer Results...
  number of frames captured: xxx

Xnor Results...
  image copy time (us): high xxx avg: xxx low:xxx frames:xxx

  buffer A image prep time (us): high:xxx avg:xxx low:xxx frames:xxx
  buffer A image eval time (us): high:xxx avg:xxx low:xxx frames:xxx

  buffer B image prep time (us): high:xxx avg:xxx low:xxx frames:xxx
  buffer B image eval time (us): high:xxx avg:xxx low:xxx frames:xxx

  <... more lines if more xnor engines are used ...>

Encoder Results...
  image copy time (us):   high:xxx avg:xxx low:xxx frames:xxx
  image encode time (us): high:xxx avg:xxx low:xxx frames:xxx

```
Of course, the 'xxx' are replaced with actual measured times and counts.  The times
are in microseconds.

In the 'Xnor Results' the 'image eval time' is the most important.  It contains the
high, average, and low times measured for an executing xnor engine.  The average
time to detect targets varies depending on how many engines you are running. See some
results below.


### Discussion

Stressor is composed of a UI thread plus three worker threads.  The worker threads are 
'capturer', 'encoder' and 'xnor'.  

The capturer is an encapsulation of a v4l2 capture module that forwards its 
captured frames to the other two worker threads.

The encoder thread waits for frames and then goes through the rigamarole required to 
run an OMX encoder loop. This thread writes the output file.

The xnor thread waits for frames and then submits them to an 'xnor engine'.  The xnor
engines are just calls to the xnor evaluation api done inside a 'std::async' function 
(so they have their own thread).  When you request more xnor engines you are simply
piling up more std::async functions with xnor evals running in them.

On a raspberry pi 3 b+ capturing 640x480@30fps and running 1 to 6 xnor engines 
for 10 seconds you get these results:

- 1 xnor engines: frames: 57,  eval time: 157ms, cpu usage: 57%
- 2 xnor engines: frames: 79,  eval time: 248ms, cpu usage: 77%
- 3 xnor engines: frames: 92,  eval time: 345ms, cpu usage: 87%
- 4 xnor engines: frames: 106, eval time: 357ms, cpu usage: 92%
- 5 xnor engines: frames: 103, eval time: 515ms, cpu usage: 95%
- 6 xnor engines: frames: 108, eval time: 555ms, cpu usage: 95%+

Notice that the sweet spot for the rpi3 is with three XNOR engines.  In that configuration 
you are processing the maximum frames (92) and using 87% of the CPU.  Piling on any
more XNOR engines doesn't lead to any improvement.  At 30 fps the system is capturing 
and encoding about 330 frames.  The XNOR module is processing about 7.3 frames a second.  That
seems pretty good on a rpi3.  :)

Update: With the release of SDK 0.9 XNOR now allows the choice of running xnor engines
in multi-threaded or single-threaded mode.  In single-threaded mode, the evaluation
times slow **way** down.  A single evaluation on the rpi3 takes 295ms.  That is about 
6x slower than with SDK 0.3.  However in multi-threaded mode the results are almost
identical to the SDK 0.3 tests.  I can only concluded that the xnor engines were running
in multi-threaded mode in SDK 0.3.

On a raspberry pi zero capturing at 640x480@30fps and running 1 to 3 xnor engines 
for 30 seconds you get this result:

- 1 xnor engines: frames: 5, eval time: 3241ms, cpu usage: 94%
- 2 xnor engines: frames: 6, eval time: 3028ms, cpu usage: 95%+
- 3 xnor engines: --crash--

The raspberry pi zero is a single core cpu and can't really keep up with a realtime
video stream.  Notice that two xnor engines shows a slight improvement but that is 
because the framerate reduced.  Surprisingly, the rpi0 makes pretty good videos. Not losing any frames.


### To Do

- Done.  ~~Try it with a USB camera~~
- Done.  ~~Switch to libyuv~~
- Done.  ~~Try it on a Raspberry pi zero (need hardware)~~
- Done.  ~~Hook up an RTSP server and send it to a phone~~
- Try it on a x86 linux AVX2 system (need hardware)
- Hook up to aws-kinesis for video stream broadcasting
